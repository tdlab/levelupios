# LevelupiOS

[![CI Status](https://img.shields.io/travis/yahengsu/LevelupiOS.svg?style=flat)](https://travis-ci.org/yahengsu/LevelupiOS)
[![Version](https://img.shields.io/cocoapods/v/LevelupiOS.svg?style=flat)](https://cocoapods.org/pods/LevelupiOS)
[![License](https://img.shields.io/cocoapods/l/LevelupiOS.svg?style=flat)](https://cocoapods.org/pods/LevelupiOS)
[![Platform](https://img.shields.io/cocoapods/p/LevelupiOS.svg?style=flat)](https://cocoapods.org/pods/LevelupiOS)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LevelupiOS is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LevelupiOS'
```

## Author

yahengsu, yahengsu@tdlab.io

## License

LevelupiOS is available under the MIT license. See the LICENSE file for more info.
