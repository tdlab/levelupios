//
//  Transfer.swift
//  VirtualBank
//
//  Created by Isaiah Erb on 2018-06-06.
//  Copyright © 2018 TD Lab. All rights reserved.
//

import Foundation

//struct Transfer: Decodable {
//    let amount: Double?
//    let categoryTags: [String]?
//    let currency: String?
//    let documentType: String?
//    let fromAccountId: String?
//    let id: String?
//    let master: Int?
//    let merchantId: String?
//    let receipt: String?
//    let toAccountId: String?
//    let transactionTime: String?
//    let transactionType: String?
//}

struct TransferResponse : Codable {
    let result : [Transfer]?
}

struct Transfer : Codable {
    let amount : Double?
    let appID : String?
    let categoryTags : TransferCategoryTags?
    let currency : String?
    let documentType : String?
    let fromAccountID : String?
    let id : String?
    let master : Int32?
    let merchantID : String?
    let receipt : String?
    let toAccountID : String?
    let transactionTime : String?
    let transactionType : String?
}

struct TransferCategoryTags : Codable {

}


