//
//  Account.swift
//  VirtualBank
//
//  Created by Omas Abdullah on 2018-05-14.
//  Copyright © 2018 TD Lab. All rights reserved.
//

import Foundation
//
//struct AccountsResponse : Codable {
//    let result: Accounts?
//}

// for error detection
struct Response : Codable {
    let errorMsg: String?
    let errorDetails: String?
    let statusCode: Int?
}

struct CustomerAccountsResponse : Codable {
    let result: Accounts?
}

struct Accounts : Codable {
    let bankAccounts: [BankAccount]?
    let creditCardAccounts: [CreditAccount]?
}

class Account : Codable {
    var type : String?
    var openDate : String? // date and time account was opened
    var id : String? // unique identifier (GUID)
    var balance : Double? // account balance
    var currency : String? // currency of the balance
}

class BankAccount : Account {
    var branchNumber: String?
    var maskedAccountNumber: String?
    var iban : String? // international bank account number
    var relatedCustomers: BankRelatedCustomers?
}

class CreditAccount : Account {
    var relatedCustomers: CreditRelatedCustomers?
    var maskedNumber: String?
    var cards: [Card]?
}

struct Card : Codable {
    let id: String?
    let securityCode: String?
    let maskedNumber: String?
    let customerId: String?
    let accountId: String?
    let nameOnCard: String?
}

struct BankRelatedCustomers : Codable {
    let individual : [Individual]?
}

struct CreditRelatedCustomers : Codable {
    let authorized : [Individual]?
}

struct  Individual : Codable {
    let customerID : String? // customer ID (GUID)
    let type : String? // customer type
}
