//
//  TDRequest.swift
//  VirtualBank
//
//  Created by Omas Abdullah on 2018-05-14.
//  Copyright © 2018 TD Lab. All rights reserved.
//

import Foundation
import UIKit

open class VirtualBank {
    
    private let defaultAuthHeader: [String: String]
    private let baseURL: URL
    private let baseURLString = "https://dev.botsfinancial.com/api"
    private var authToken: String
    
    private static var instance: VirtualBank?
    
    private init(authToken: String) {
        self.baseURL = URL(string: baseURLString)!
        self.authToken = authToken
        self.defaultAuthHeader = ["Authorization" : authToken]
    }
    
    public class func setToken(_ authToken: String) {
        instance = VirtualBank(authToken: authToken)
    }
    
    public class func getInstance() -> VirtualBank? {
        guard let instance = instance else {
            print("Error: tried to access virtual bank instance without initializing the auth token")
            return nil
        }
        return instance
    }
    
    private func getErrorType(_ status: Int?) -> TDError {
        if status == nil {
            return .generic
        }
        
        switch status {
        case 401: return .unauthorized
        case 403: return .forbidden
        case 404: return .notFound
        default: return .generic
        }
        
    }
    
    public func getBankAccountBy(accountID: String, success: @escaping (BankAccount?) -> Void, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { // works
        getRequest(from: "/accounts/\(accountID)", success: { (data) in
            do {
                let response = try JSONDecoder().decode(AccountResponse.self, from: data)
                if let bankAccount = response.result?.bankAccount {
                    success(bankAccount)
                } else {
                    failure?(TDError.generic, UIAlertController.errorAlert(message: "Response did not return a valid Bank Account"))
                }
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    
    public func getCreditCardAccountBy(accountID: String, success: @escaping (CreditCardAccount?) -> Void, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { // works
        getRequest(from: "/accounts/\(accountID)", success: { (data) in
            do {
                let response = try JSONDecoder().decode(AccountResponse.self, from: data)
                if let creditCardAccount = response.result?.creditCardAccount {
                    success(creditCardAccount)
                } else {
                    failure?(TDError.generic, UIAlertController.errorAlert(message: "Response did not return a valid Bank Account"))
                }
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    public func getTransactionsBy(accountID: String,  success: @escaping ([Transaction]?) -> Void, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { // works
        getRequest(from: "/accounts/\(accountID)/transactions", success: { (data) in
            do {
                let response = try JSONDecoder().decode(AccountTransactionsResponse.self, from: data)
                success(response.result)
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    public func getAppAccount(success: @escaping (Account?) -> Void, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { // maybe works, app not found error right now
        getRequest(from: "/accounts/self", success: { (data) in
            do {
                let response = try JSONDecoder().decode(AppAccountResponse.self, from: data)
                success(response.result)
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    public func patchAppAccount(newBalance: Double, success: ((Account?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { // maybe works, but cannot test without app
        
        let parameters: [String: Any] = ["balance": newBalance]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) else {
            print("Could not serialize json post transfer request")
            failure?(TDError.jsonEncoding, UIAlertController.errorAlert(message: TDError.jsonEncoding.localizedDescription))
            return
        }
        
        sendPatch(to: "/accounts/self", with: jsonData, success: { (data) in
            do {
                let response = try JSONDecoder().decode(AppAccountResponse.self, from: data)
                success?(response.result)
            } catch {
                print("Unexpected error: \(error).")
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
            
        }
    }
    
    public func getBranches(success: @escaping ([Branch]?) -> Void, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
        getRequest(from: "/branches", success: { (data) in
            do { // works
                let response = try JSONDecoder().decode(BranchesResponse.self, from: data)
                success(response.result)
            } catch {
                print("Unexpected error: \(error).")
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    public func getBranchBy(branchID: String, success: @escaping (Branch?) -> Void, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { //works
        getRequest(from: "/branches/\(branchID)", success: { (data) in
            do {
                let response = try JSONDecoder().decode(BranchesResponse.self, from: data)
                guard let result = response.result,
                    result.count >= 1 else {
                        failure?(TDError.badData, UIAlertController.errorAlert(message: TDError.badData.localizedDescription))
                        return
                }
                success(result[0])
            } catch {
                print("Unexpected error: \(error).")
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    public func getCustomerBy(customerID: String, success: @escaping (Customer?) -> Void, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { //works
        getRequest(from: "/customers/\(customerID)", success: { (data) in
            do {
                let response = try JSONDecoder().decode(CustomerResponse.self, from: data)
                guard let result = response.result,
                    result.count >= 1 else {
                        failure?(TDError.badData, UIAlertController.errorAlert(message: TDError.badData.localizedDescription))
                        return
                }
                success(result[0])
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    public func getAccountsBy(customerID: String, success: @escaping ([Account]?) -> Void, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { //works
        getRequest(from: "/customers/\(customerID)/accounts", success: { (data) in
            do {
                let response = try JSONDecoder().decode(CustomerAccountsResponse.self, from: data)
                guard let accounts = response.result,
                    let bankAccounts = accounts.bankAccounts,
                    let creditAccounts = accounts.creditCardAccounts else {
                        failure?(TDError.parsing, UIAlertController.errorAlert(message: TDError.parsing.localizedDescription))
                        return
                }
                var accountUnion: [Account] = []
                accountUnion.append(contentsOf: bankAccounts)
                accountUnion.append(contentsOf: creditAccounts)
                
                success(accountUnion)
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    public func getBankAccountsBy(customerID: String, success: @escaping ([BankAccount]?) -> Void, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { // works
        getRequest(from: "/customers/\(customerID)/accounts", success: { (data) in
            do {
                let response = try JSONDecoder().decode(CustomerAccountsResponse.self, from: data)
                guard let accounts = response.result,
                    let bankAccounts = accounts.bankAccounts else {
                        failure?(TDError.parsing, UIAlertController.errorAlert(message: TDError.parsing.localizedDescription))
                        return
                }
                success(bankAccounts)
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    
    public func getCreditCardAccountsBy(customerID: String, success: @escaping ([CreditCardAccount]?) -> Void, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { // works
        getRequest(from: "/customers/\(customerID)/accounts", success: { (data) in
            do {
                let response = try JSONDecoder().decode(CustomerAccountsResponse.self, from: data)
                guard let accounts = response.result,
                    let creditAccounts = accounts.creditCardAccounts else {
                        failure?(TDError.parsing, UIAlertController.errorAlert(message: TDError.parsing.localizedDescription))
                        return
                }
                success(creditAccounts)
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    
    
    public func getTransactionsBy(customerID: String, success: @escaping ([Transaction]?) -> Void, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { // gives array index out of bounds exception on server side
        getRequest(from: "/customers/\(customerID)/transactions", success: { (data) in
            do {
                let response = try JSONDecoder().decode(CustomerTransactionsResponse.self, from: data)
                success(response.result)
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    public func getTransactionBy(transactionID: String, success: @escaping (Transaction?) -> Void, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { //works
        getRequest(from: "/transactions/\(transactionID)", success: { (data) in
            do {
                let response = try JSONDecoder().decode(TransactionResponse.self, from: data)
                guard let result = response.result,
                    result.count >= 1 else {
                        failure?(TDError.badData, UIAlertController.errorAlert(message: TDError.badData.localizedDescription))
                        return
                }
                success(result[0])
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    public func getTransferBy(transferID: String, success: @escaping (Transfer?) -> Void , failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) { // works with a transaction id but is null, where can I find a transfer ID?
        getRequest(from: "/transactions/\(transferID)", success: { (data) in
            do {
                let response = try JSONDecoder().decode(TransferResponse.self, from: data)
                guard let result = response.result,
                    result.count >= 1 else {
                        failure?(TDError.badData, UIAlertController.errorAlert(message: TDError.badData.localizedDescription))
                        return
                }
                success(result[0])
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    private func getRequest(from urlExtension: String, success: ((Data) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> (Void))? = nil) {
        guard let url = URL(string: "\(baseURL)\(urlExtension)") else {
            failure?(TDError.incorrectURL, UIAlertController.errorAlert(message: TDError.incorrectURL.localizedDescription))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let headers = ["Authorization": authToken,
                       "Content-Type": "application/json"]
        request.allHTTPHeaderFields = headers
        
        
        URLSession.shared.invalidateAndCancel()
        
        let task = URLSession.shared.dataTask(with: request) { data, urlResponse, error in
            
            if let error = error {
                failure?(TDError.generic, UIAlertController.errorAlert(message: error.localizedDescription))
                return
            }
            
            guard let data = data else {
                failure?(TDError.badData, UIAlertController.errorAlert(message: "error code: \(TDError.badData)"))
                return
            }
            
            if let httpResponse = urlResponse as? HTTPURLResponse, httpResponse.statusCode != 200 {
                // check for http errors
                let statusCode = httpResponse.statusCode
                
                // try to get specialized message
                do {
                    let formattedResponse = try JSONDecoder().decode(Response.self, from: data)
                    let errorMsg = formattedResponse.errorMsg
                    let errorDetails = formattedResponse.errorDetails
                    failure?(self.getErrorType(statusCode), UIAlertController.errorAlert(message: "\(errorMsg ?? "")\(errorDetails==nil ? "" : "\n")\(errorDetails ?? "")"))
                } catch {
                    failure?(self.getErrorType(statusCode), UIAlertController.errorAlert(message: self.getErrorType(statusCode).localizedDescription))
                }
                return
            }
            
            
            success?(data)
        }
        task.resume()
    }
    
    
    public func postTransfer(of amount: Double, from sender: String, to receiver: String, success: (()->())? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> (Void))? = nil) { // invalid account id error
        
        guard let url = URL(string: "\(baseURL)/transfers") else {
            failure?(TDError.incorrectURL, UIAlertController.errorAlert(message: TDError.incorrectURL.localizedDescription))
            return
        }
        
        let headers = ["Authorization": authToken,
                       "Content-Type": "application/json"]
        
        let parameters: [String: Any] = ["amount": amount,
                                         "currency": "CAD",
                                         "fromAccountID": sender,
                                         "receipt": "null",
                                         "toAccountID": receiver]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) else {
            print("Could not serialize json post transfer request")
            failure?(TDError.jsonEncoding, UIAlertController.errorAlert(message: TDError.jsonEncoding.localizedDescription))
            return
        }
        
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = jsonData
        
        URLSession.shared.invalidateAndCancel()
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                let error = error
                failure?(TDError.generic, UIAlertController.errorAlert(message: error?.localizedDescription))
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
                // check for http errors
                let statusCode = httpResponse.statusCode
                
                // try to get specialized message
                do {
                    let formattedResponse = try JSONDecoder().decode(Response.self, from: data)
                    let errorMsg = formattedResponse.errorMsg
                    let errorDetails = formattedResponse.errorDetails
                    failure?(self.getErrorType(statusCode), UIAlertController.errorAlert(message: "\(errorMsg ?? "")\(errorDetails==nil ? "\n" : "\n")\(errorDetails ?? "")"))
                } catch {
                    failure?(self.getErrorType(statusCode), UIAlertController.errorAlert(message: self.getErrorType(statusCode).localizedDescription))
                }
                return
            }
            
            success?()
        }
        
        task.resume()
    }
    
    private func sendPatch(to urlExtension: String, with jsonData: Data, success: ((Data) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> (Void))? = nil) {
        guard let url = URL(string: "\(baseURL)\(urlExtension)") else {
            failure?(TDError.incorrectURL, UIAlertController.errorAlert(message: TDError.incorrectURL.localizedDescription))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "PATCH"
        let headers = ["Authorization": authToken,
                       "Content-Type": "application/json"]
        request.allHTTPHeaderFields = headers
        request.httpBody = jsonData
        
        URLSession.shared.invalidateAndCancel()
        
        let task = URLSession.shared.dataTask(with: request) { data, urlResponse, error in
            
            if let error = error {
                print(error)
                failure?(TDError.generic, UIAlertController.errorAlert(message: error.localizedDescription))
                return
            }
            
            guard let data = data else {
                failure?(TDError.badData, UIAlertController.errorAlert(message: "error code: \(TDError.badData)"))
                return
            }
            
            if let httpResponse = urlResponse as? HTTPURLResponse {
                // check for http errors
                let statusCode = httpResponse.statusCode
                var formattedResponse: Response
                
                do {
                    formattedResponse = try JSONDecoder().decode(Response.self, from: data)
                } catch {
                    failure?(self.getErrorType(statusCode), UIAlertController.errorAlert(message: self.getErrorType(statusCode).localizedDescription))
                    return
                }
                
                let errorMsg = formattedResponse.errorMsg
                let errorDetails = formattedResponse.errorDetails
                
                if statusCode != 200 || (errorMsg != nil && !errorMsg!.isEmpty) || (errorDetails != nil && !errorDetails!.isEmpty) {
                    failure?(self.getErrorType(statusCode), UIAlertController.errorAlert(message: "\(errorMsg ?? "")\(errorDetails==nil ? "" : "\n")\(errorDetails ?? "")"))
                }
                
            } else {
                failure?(TDError.generic, UIAlertController.errorAlert(message: "Could not generate a response."))
                return
            }
            
            success?(data)
        }
        
        task.resume()
    }
    
}
