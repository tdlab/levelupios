//
//  TDRequest.swift
//  VirtualBank
//
//  Created by Omas Abdullah on 2018-05-14.
//  Copyright © 2018 TD Lab. All rights reserved.
//

import Foundation
import UIKit

class VirtualBank {
    
    private let defaultAuthHeader: [String: String]
    private let baseURL: URL
    private let baseURLString = "https://dev.botsfinancial.com/api"
    private var authToken: String
    
    private static var instance: VirtualBank?
    
    private init(authToken: String) {
        self.baseURL = URL(string: baseURLString)!
        self.authToken = authToken
        self.defaultAuthHeader = ["Authorization" : authToken]
    }
    
    public class func setToken(_ authToken: String) {
        instance = VirtualBank(authToken: authToken)
    }
    
    public class func getInstance() -> VirtualBank? {
        guard let instance = instance else {
            print("Error: tried to access virtual bank instance without initializing the auth token")
            return nil
        }
        return instance
    }
    
    private func getErrorType(_ status: Int?) -> TDError {
        if status == nil {
            return .generic
        }
        
        switch status {
        case 401: return .unauthorized
        case 403: return .forbidden
        case 404: return .notFound
        default: return .generic
        }
    }
    
//    func getAccounts(success: (([DepositAccount]?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
//        getRequest(with: "/accounts", success: { (data) in
//            do {
//                let response = try JSONDecoder().decode(AccountsResponse.self, from: data)
//                success?(response.result)
//            } catch {
//                print("Unexpected error: \(error).")
//            }
//        }) { (error, alertController) -> (Void) in
//            failure?(error, alertController)
//        }
//    }
    
//    func getAccountBy(id: String, success: ((DepositAccount?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
//        getRequest(with: "/accounts/\(id)", success: { (data) in
//            do {
//                let response = try JSONDecoder().decode(AccountResponse.self, from: data)
//                guard let result = response.result,
//                    result.count >= 1 else {
//                    failure?(TDError.badData, UIAlertController.errorAlert(message: TDError.badData.localizedDescription))
//                        return
//                }
//                success?(result[0])
//            } catch {
//                print("Unexpected error: \(error).")
//            }
//        }) { (error, alertController) -> (Void) in
//            failure?(error, alertController)
//        }
//    }
    
    
    func getAccountTransactionsBy(id: String,  success: (([Transaction]?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
        getRequest(with: "/accounts/\(id)/transactions", success: { (data) in
            do {
                let response = try JSONDecoder().decode(AccountTransactionsResponse.self, from: data)
                success?(response.result)
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    func getBranches(success: (([Branch]?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
        getRequest(with: "/branches", success: { (data) in
            do {
                let response = try JSONDecoder().decode(BranchesResponse.self, from: data)
                success?(response.result)
            } catch {
                print("Unexpected error: \(error).")
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    func getBranchBy(id: String, success: ((Branch?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
        getRequest(with: "/branches/\(id)", success: { (data) in
            do {
                let response = try JSONDecoder().decode(BranchesResponse.self, from: data)
                guard let result = response.result,
                    result.count >= 1 else {
                        failure?(TDError.badData, UIAlertController.errorAlert(message: TDError.badData.localizedDescription))
                        return
                }
                success?(result[0])
            } catch {
                print("Unexpected error: \(error).")
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
//    func getCustomers(success: (([Customer]?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
//        getRequest(with: "/customers", success: { (data) in
//            do {
//                let response = try JSONDecoder().decode(CustomersResponse.self, from: data)
//                success?(response.result)
//            } catch {
//                print("Unexpected error: \(error).")
//            }
//        }) { (error, alertController) -> (Void) in
//            failure?(error, alertController)
//        }
//    }
    
    func getCustomerBy(id: String, success: ((Customer?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
        getRequest(with: "/customers/\(id)", success: { (data) in
            do {
                print(data.toJSON(data))
                let response = try JSONDecoder().decode(CustomerResponse.self, from: data)
                guard let result = response.result,
                    result.count >= 1 else {
                        failure?(TDError.badData, UIAlertController.errorAlert(message: TDError.badData.localizedDescription))
                        return
                }
                success?(result[0])
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    func getCustomerAccountsBy(id: String, success: (([Account]?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
        getRequest(with: "/customers/\(id)/accounts", success: { (data) in
            do {
                let response = try JSONDecoder().decode(CustomerAccountsResponse.self, from: data)
                guard let accounts = response.result,
                    let bankAccounts = accounts.bankAccounts,
                    let creditAccounts = accounts.creditCardAccounts else {
                        failure?(TDError.parsing, UIAlertController.errorAlert(message: TDError.parsing.localizedDescription))
                        return
                }
                var accountUnion: [Account] = []
                accountUnion.append(contentsOf: bankAccounts)
                accountUnion.append(contentsOf: creditAccounts)
                
                success?(accountUnion)
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    
    func getCustomerBankAccountsBy(id: String, success: (([BankAccount]?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
        getRequest(with: "/customers/\(id)/accounts", success: { (data) in
            do {
                let response = try JSONDecoder().decode(CustomerAccountsResponse.self, from: data)
                guard let accounts = response.result,
                    let bankAccounts = accounts.bankAccounts else {
                    failure?(TDError.parsing, UIAlertController.errorAlert(message: TDError.parsing.localizedDescription))
                    return
                }
                success?(bankAccounts)
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    
    func getCustomerCreditAccountsBy(id: String, success: (([CreditAccount]?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
        getRequest(with: "/customers/\(id)/accounts", success: { (data) in
            do {
                let response = try JSONDecoder().decode(CustomerAccountsResponse.self, from: data)
                guard let accounts = response.result,
                    let creditAccounts = accounts.creditCardAccounts else {
                        failure?(TDError.parsing, UIAlertController.errorAlert(message: TDError.parsing.localizedDescription))
                        return
                }
                success?(creditAccounts)
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    
    
    func getCustomerTransactionsBy(id: String, success: (([Transaction]?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
        getRequest(with: "/customers/\(id)/transactions", success: { (data) in
            do {
                let response = try JSONDecoder().decode(CustomerTransactionsResponse.self, from: data)
                success?(response.result)
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    func getTransactionBy(id: String, success: ((Transaction?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
        getRequest(with: "/transactions/\(id)", success: { (data) in
            do {
                let response = try JSONDecoder().decode(TransactionResponse.self, from: data)
                guard let result = response.result,
                    result.count >= 1 else {
                        failure?(TDError.badData, UIAlertController.errorAlert(message: TDError.badData.localizedDescription))
                        return
                }
                success?(result[0])
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    func getTransferBy(id: String, success: ((Transfer?) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> Void)? = nil) {
        getRequest(with: "/transactions/\(id)", success: { (data) in
            do {
                let response = try JSONDecoder().decode(TransferResponse.self, from: data)
                guard let result = response.result,
                    result.count >= 1 else {
                        failure?(TDError.badData, UIAlertController.errorAlert(message: TDError.badData.localizedDescription))
                        return
                }
                success?(result[0])
            } catch {
                print("Unexpected error: \(error).")
                failure?(TDError.parsing, UIAlertController.errorAlert(message: error.localizedDescription))
            }
        }) { (error, alertController) -> (Void) in
            failure?(error, alertController)
        }
    }
    
    func getRequest(with urlExtension: String, success: ((Data) -> Void)? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> (Void))? = nil) {
        guard let url = URL(string: "\(baseURL)\(urlExtension)") else {
            failure?(TDError.incorrectURL, UIAlertController.errorAlert(message: TDError.incorrectURL.localizedDescription))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let headers = ["Authorization": authToken,
                       "Content-Type": "application/json"]
        request.allHTTPHeaderFields = headers

        
        URLSession.shared.invalidateAndCancel()
        
        let task = URLSession.shared.dataTask(with: request) { data, urlResponse, error in
            
            if let error = error {
                print(error)
                failure?(TDError.generic, UIAlertController.errorAlert(message: error.localizedDescription))
                return
            }
            
            guard let data = data else {
                failure?(TDError.badData, UIAlertController.errorAlert(message: "error code: \(TDError.badData)"))
                return
            }
            
            if let httpResponse = urlResponse as? HTTPURLResponse, httpResponse.statusCode != 200 {
                // check for http errors
                let statusCode = httpResponse.statusCode
                
                // try to get specialized message
                do {
                    let formattedResponse = try JSONDecoder().decode(Response.self, from: data)
                    let errorMsg = formattedResponse.errorMsg
                    let errorDetails = formattedResponse.errorDetails
                    failure?(self.getErrorType(statusCode), UIAlertController.errorAlert(message: "\(errorMsg ?? "")\(errorDetails==nil ? "" : "\n")\(errorDetails ?? "")"))
                } catch {
                    failure?(self.getErrorType(statusCode), UIAlertController.errorAlert(message: self.getErrorType(statusCode).localizedDescription))
                }
                return
            }
            

            success?(data)
        }
        task.resume()
    }
    
    
    func postTransfer(of amount: Double, from sender: String, to receiver: String, success: (()->())? = nil, failure: ((_ error: TDError, _ errorAlert: UIAlertController) -> (Void))? = nil) {
      
        guard let url = URL(string: "\(baseURL)/transfers") else {
            failure?(TDError.incorrectURL, UIAlertController.errorAlert(message: TDError.incorrectURL.localizedDescription))
            return
        }
        
        let headers = ["Authorization": authToken,
                       "Content-Type": "application/json"]
        
        let parameters: [String: Any] = ["amount": amount,
                                      "currency": "CAD",
                                      "fromAccountID": sender,
                                      "receipt": "null",
                                      "toAccountID": receiver]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) else {
            print("Could not serialize json post transfer request")
            failure?(TDError.jsonEncoding, UIAlertController.errorAlert(message: TDError.jsonEncoding.localizedDescription))
            return
        }
        
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = jsonData
        
        URLSession.shared.invalidateAndCancel()
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            print(error)
            guard let data = data, error == nil else {
                // check for fundamental networking error
                let error = error
                failure?(TDError.generic, UIAlertController.errorAlert(message: error?.localizedDescription))
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
                // check for http errors
                let statusCode = httpResponse.statusCode
                
                // try to get specialized message
                do {
                    let formattedResponse = try JSONDecoder().decode(Response.self, from: data)
                    let errorMsg = formattedResponse.errorMsg
                    let errorDetails = formattedResponse.errorDetails
                    failure?(self.getErrorType(statusCode), UIAlertController.errorAlert(message: "\(errorMsg ?? "")\(errorDetails==nil ? "\n" : "\n")\(errorDetails ?? "")"))
                } catch {
                    failure?(self.getErrorType(statusCode), UIAlertController.errorAlert(message: self.getErrorType(statusCode).localizedDescription))
                }
                return
            }

            success?()
        }

        task.resume()
    }
    
}
