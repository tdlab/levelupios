//
//  SwiftExtensions.swift
//  VirtualBank
//
//  Created by Omas Abdullah on 2018-05-14.
//  Copyright © 2018 TD Lab. All rights reserved.
//

import UIKit


extension UIAlertController {
    class func errorAlert(message: String?) -> UIAlertController {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        return alertController
    }
}
