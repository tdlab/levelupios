

Pod::Spec.new do |s|
  s.name             = 'LevelupiOS'
  s.version          = '1.0.3'
  s.summary          = 'iOS SDK for TD LevelUp'

  s.description      = <<-DESC
iOS SDK for TD LevelUp Event
                       DESC

  s.homepage         = 'https://www.td.com'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'yahengsu' => 'yahengsu@tdlab.io' }
  s.source           = { :git => 'http://bitbucket.org/tdlab/levelupios.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '11.3'
  s.swift_version = '4.1'

  s.source_files = 'LevelupiOS/Classes/*'
end
