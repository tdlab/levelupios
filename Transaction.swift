//
//  Transaction.swift
//  VirtualBank
//
//  Created by Isaiah Erb on 2018-06-06.
//  Copyright © 2018 TD Lab. All rights reserved.
//

import Foundation


struct AccountTransactionsResponse : Codable {
    let result: [Transaction]?
}

struct CustomerTransactionsResponse : Codable {
    let result: [Transaction]?
}

struct TransactionResponse : Codable {
    let result: [Transaction]?
}

struct Transaction : Codable {
    let accountID : String?
    let appID : String?
    let categoryTags : TransactionCategoryTags?
    let currencyAmount : Double?
    let customerID : String?
    let description : String?
    let documentType : String?
    let effectiveDate : String?
    let id : String?
    let locationCity : String?
    let locationCountry : String?
    let locationLatitude : Double?
    let locationLongitude : Double?
    let locationPostalCode : String?
    let locationRegion : String?
    let locationStreet : String?
    let master : Int32?
    let merchantCategoryCode : String?
    let merchantID : String?
    let merchantName : String?
    let originalCurrencyAmount : Double?
    let originalCurrency : String?
    let originiationDate : String?
    let postBalance : Double?
    let postDate : String?
    let source : String?
    let type : String?
}

struct TransactionCategoryTags : Codable {
    
}
