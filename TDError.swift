//
//  TDError.swift
//  VirtualBank
//
//  Created by Isaiah Erb on 2018-06-06.
//  Copyright © 2018 TD Lab. All rights reserved.
//

import Foundation

enum TDError: Error {
    case incorrectURL
    case unauthorized
    case forbidden
    case notFound
    case generic
    case badData
    case parsing
    case jsonEncoding
}

extension TDError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .unauthorized:
            return NSLocalizedString("401: Unauthorized error occured.", comment: "401 error")
        case .forbidden:
            return NSLocalizedString("403: Forbidden error occured.", comment: "403 error")
        case .notFound:
            return NSLocalizedString("404: Not found error occured.", comment: "404 error")
        case .generic:
            return NSLocalizedString("An unknown error occured.", comment: "Generic error")
        case .incorrectURL:
            return NSLocalizedString("The URL was invalid.", comment: "URL Parsing error")
        case .badData:
            return NSLocalizedString("The response data was misformed or nil.", comment: "Response Error")
        case .parsing:
            return NSLocalizedString("The response data could not be parsed into a Codable", comment: "JSON ParsingError")
        case .jsonEncoding:
            return NSLocalizedString("The body of the request could not be serialized, fix formatting", comment: "JSON Serialization Error")
        }
    }
}
